from django.contrib import admin
from account.models import Account, Resource, Group


# admin.register(Account)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    """
    账号后台管理
    """
    list_display = ('username',)  # 展示
    list_filter = ('username',)
    list_per_page = 20  # 分页
    search_fields = ('username',)


@admin.register(Resource)
class ResourceAdmin(admin.ModelAdmin):
    """
    资源后台管理
    """
    list_display = ('id', 'name', 'url', 'pid', 'status')  # 展示
    list_filter = ('name', 'url', 'status')
    list_per_page = 20  # 分页
    search_fields = ('name', 'url', 'status')


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    """
    组资源管理
    """
    list_display = ('name', )  # 展示
    list_filter = ('name', )
    list_per_page = 20  # 分页
    search_fields = ('name', )
