"""
account中公用方法
"""
from account.models import Group
from account.serializers import ResourceSerializer


def get_resource_list(user):
    """
    获取资源列表
    """
    group_list = Group.objects.filter(user=user)
    resource_list = []
    for group in group_list:
        resource = ResourceSerializer(group.resource.filter(status=1).all(), many=True).data
        for data in resource:
            if resource_list.count(data) == 0:
                resource_list.append(data)
    return resource_list



