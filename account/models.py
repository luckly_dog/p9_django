from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractUser


class Account(AbstractUser):
    """
    创建用户模型
    """
    phone = models.CharField(max_length=13, blank=True, null=True, verbose_name='手机号')
    status = models.IntegerField(default=1, verbose_name='维修人员状态')

    def __str__(self):
        return self.username

    class Meta:
        """
        重命名
        """
        db_table = 'account_account'

    @classmethod
    def check_phone(cls, number):
        """
        验证手机号是否被注册

        return False  验证失败    True 验证通过
        """
        user = cls.objects.filter(phone=number).first()
        if not user:
            return True
        return False

    def update_status(self, status):
        """
        更新状态
        status
        """
        self.status = status
        self.save()


class AccountInfo(models.Model):
    """
    用户信息表
    """
    # account = models.ForeignKey(Account)
    pass

class Resource(models.Model):
    """
    权限 资源表

    用户可以访问哪些前端页面
    1、url = '/mail'  管理员
    2、url = '/add_mail' 所有登录用户展示
    3、ulr = '/show_car' 管理员
    """
    name = models.CharField(max_length=50, verbose_name='权限描述', default='')
    url = models.CharField(max_length=200, verbose_name='url地址', default='')
    status = models.IntegerField(default=1, verbose_name='是否可用1可用 0不可用')
    pid = models.IntegerField(verbose_name='父id', default='0')

    def __str__(self):
        return "{}:{}".format(self.name, self.url)

    class Meta:
        """
        重命名
        """
        db_table = 'account_resource'


class Group(models.Model):
    """
    权限组

    权限组一：1、2、3
    权限组二：2、3
    """
    name = models.CharField(max_length=50, verbose_name='权限组描述', default='')
    user = models.ManyToManyField(Account)
    resource = models.ManyToManyField(Resource)

    def __str__(self):
        return self.name

    class Meta:
        """
        重命名
        """
        db_table = 'account_group'


