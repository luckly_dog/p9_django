"""
序列化器
"""

from rest_framework import serializers
from account.models import Resource


class ResourceSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """

    class Meta:
        """
        序列化模型
        """
        model = Resource
        fields = '__all__'

