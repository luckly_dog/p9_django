
from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from account import views

urlpatterns = [
    path('login', obtain_jwt_token, name='login'),
    path('logout', views.LogOut.as_view(), name='logout'),
    path('resource_list', views.GetUserResource.as_view(), name='resource'),
    path('send_mail', views.SendMail.as_view(), name='send_mail'),
    path('forget', views.ForgetPassword.as_view(), name="forget_pwd"),
    path('valid_email', views.validEmail.as_view(), name="valid_email"),
    path('get_qiniu_token', views.GetQiniuToken.as_view(), name="get_qiniu_token"),
]
