"""
序列化器
"""

from rest_framework import serializers
from announcement.models import Announcement


class AnnouncementSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """

    class Meta:
        """
        序列化模型
        """
        model = Announcement
        fields = '__all__'

