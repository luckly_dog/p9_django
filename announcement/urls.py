
from django.urls import path

from announcement import views

urlpatterns = [
    path('get_announcement', views.GetAnnouncementList.as_view(), name='get_announcement'),

]
