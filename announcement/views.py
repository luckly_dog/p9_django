from django.shortcuts import render

from announcement.models import Announcement
from rest_framework.views import APIView
from rest_framework.response import Response
from announcement.serializers import AnnouncementSerializer


# Create your views here.

class GetAnnouncementList(APIView):
    """
    获取公告列表
    """

    def get(self):
        result = Announcement.objects.order_by('-id').all()
        result = AnnouncementSerializer(result, many=True)
        return Response({'code': 200, 'data': result})
