from django.contrib import admin
from car.models import Car, Record, TypeMoney

admin.register(Record)
admin.register(TypeMoney)


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = ('name', 'car_number')  # 展示
    list_filter = ('car_number',)
    list_per_page = 4  # 分页
    search_fields = ('car_number',)

