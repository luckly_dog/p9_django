# Generated by Django 3.2.13 on 2022-05-11 09:45

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='车主名称')),
                ('car_number', models.CharField(max_length=30, unique=True, verbose_name='车牌号')),
                ('type', models.IntegerField(default=2, verbose_name='1包月 2非包月')),
            ],
            options={
                'db_table': 'car',
            },
        ),
        migrations.CreateModel(
            name='TypeMoney',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.IntegerField(default=3, verbose_name='1包月 2业主  3非业主')),
                ('money', models.DecimalField(decimal_places=2, max_digits=5, verbose_name='每小时单价')),
            ],
            options={
                'db_table': 'type_money',
            },
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('in_time', models.DateTimeField(default=django.utils.timezone.now, verbose_name='进入时间')),
                ('out_time', models.DateTimeField(verbose_name='离开时间')),
                ('money', models.DecimalField(decimal_places=2, max_digits=5, verbose_name='价格')),
                ('car_number', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='car.car')),
            ],
            options={
                'db_table': 'record',
            },
        ),
    ]
