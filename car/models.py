"""
车辆相关的模型表
"""
import decimal

from django.db import models
from django.utils import timezone


class Car(models.Model):
    """
    车辆表
    """
    name = models.CharField(max_length=30, verbose_name='车主名称')
    car_number = models.CharField(max_length=30, unique=True, verbose_name='车牌号')
    type = models.IntegerField(default=2, verbose_name='1包月 2非包月')
    # expire_time = models.DateTimeField(verbose_name='到期时间')

    class Meta:
        """
        重命名
        """
        db_table = 'car'


class TypeMoney(models.Model):
    """
    价位表
    """
    type = models.IntegerField(default=3, verbose_name='1包月 2业主  3非业主')
    money = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='每小时单价')

    class Meta:
        """
        重命名
        """
        db_table = 'type_money'


class Record(models.Model):
    """
    记录出入表
    """
    car_number = models.CharField(max_length=30, verbose_name='车牌号', default='京F00000')
    in_time = models.DateTimeField(default=timezone.now, verbose_name='进入时间')
    out_time = models.DateTimeField(verbose_name='离开时间', blank=True, null=True)
    money = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='价格', blank=True, null=True)

    class Meta:
        """
        重命名
        """
        db_table = 'record'

    def to_json(self):
        return {
            'id': self.id,
            'car': self.car_number,
            'in_time': str(self.in_time.strftime('%Y-%m-%d %H:%M:%S') if self.in_time else '-'),
            'out_time': str(self.out_time.strftime('%Y-%m-%d %H:%M:%S') if self.out_time else '-'),
            'money': str(self.money if self.money else '-')

        }
