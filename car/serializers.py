"""
序列化器
"""

from rest_framework import serializers
from car.models import Record


class RecordSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """

    class Meta:
        """
        序列化模型
        """
        model = Record
        fields = '__all__'


class RecordListSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """
    car = serializers.SerializerMethodField()
    in_time = serializers.SerializerMethodField()
    out_time = serializers.SerializerMethodField()
    money = serializers.SerializerMethodField()

    class Meta:
        """
        序列化模型
        """
        model = Record
        fields = '__all__'

    def get_car(self, obj):
        return obj.car_number

    def get_in_time(self, obj):
        return str(obj.in_time.strftime('%Y-%m-%d %H:%M:%S') if obj.in_time else '-')

    def get_out_time(self, obj):
        return str(obj.out_time.strftime('%Y-%m-%d %H:%M:%S') if obj.out_time else '-')

    def get_money(self, obj):
        return str(obj.money if obj.money else '-')
