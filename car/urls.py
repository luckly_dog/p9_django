
from django.urls import path

from car import views

urlpatterns = [
    path('into', views.AddRecord.as_view(), name='into'),
    path('out', views.leave_place, name='out'),
    path('add_car', views.AddCar.as_view(), name='add_car'),
    path('info/query', views.get_car_data, name='get_car_data'),
    path('record/query',  views.RecordList.as_view(), name='get_car_record'),
]
