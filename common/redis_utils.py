"""
redis封装
"""
import json
import traceback

import redis
import demjson
from mysite.settings import REDIS_IP, REDIS_PORT, DB_INDEX


class RDS(object):
    """
    封装redis
    """

    def __init__(self):
        self.redis = redis.Redis(host=REDIS_IP, port=REDIS_PORT, db=DB_INDEX)

    def left_push(self, key, *value):
        """
        执行lpush操作
        从队列左侧插入数据
        key string
        value string
        """
        try:
            self.redis.lpush(key, *value)
            return self.get_list(key)
        except:
            error = traceback.format_exc()
            return error

    def right_push(self, key, *value):
        """
        从右侧插入数据
        """
        try:
            self.redis.rpush(key, *value)
            return self.get_list(key)
        except:
            error = traceback.format_exc()
            return error

    def left_pop(self, key):
        """
        从左侧弹出单个数据
        """
        try:
            value = self.redis.lpop(key)
            if value:
                value = value.decode('utf-8')
                return value
            return False
        except:
            error = traceback.format_exc()
            print('left_pop', error)
            return False

    def right_pop(self, key):
        """
        从左侧弹出单个数据
        """
        try:
            value = self.redis.rpop(key)
            if value:
                value = value.decode('utf-8')
                return value
            return False
        except:
            error = traceback.format_exc()
            print('right_pop', error)
            return False

    def set_rds(self, name, key, value):
        self.redis.hset(name, key, value)

    def get_rds(self, name, key):
        return self.redis.hget(name, key)

    def get_list(self, key):
        """
        获取队列数据
        """
        try:
            value = self.redis.lrange(key, 0, -1)
            return value
        except:
            error = traceback.format_exc()
            print('get_list', error)
            return []

    def get_hash(self, name, key):
        return self.redis.hget(name, key)

    def set_hash(self, name, mapping):
        """
        保存hash数据
        """
        try:
            self.redis.hmset(name, mapping=mapping)
            # self.redis.save()
            return True
        except:
            error = traceback.format_exc()
            print('set_hash', error)
            return False

    def update_hash_count(self, name, key, count):
        """
        redis 加减数量
        """
        # TODO 数量异常如何处理
        self.redis.hincrby(name, key, count)
        # self.redis.save()

    def get_hash_all(self, name):
        """
        获取所有数据
        """
        result = {}
        data = self.redis.hgetall(name)
        for key, value in data.items():
            result.update({json.loads(key): json.loads(value)})
        return result

    def del_hash(self, name, key):
        """
        删除key
        """
        self.redis.hdel(name, key)
        # self.redis.save()
