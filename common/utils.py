"""
公用方法
"""

from qiniu import Auth
from rest_framework.pagination import PageNumberPagination
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
secret_key = 'lhYcCubuejZfMWplnR1zzSal_R1NqckDw6GagEOI'
access_key = 'eF4FbTEmJgappCTh005ZhnLRUCSFuaMSdsRq6IJy'


def Encrypt(value):
    data = signing.dumps(value)
    data = signing.b64_encode(data.encode()).decode()
    return data


def Decrypt(value):
    data = signing.b64_decode(value.encode()).decode()
    data = signing.loads(data)
    return data


def general(user):
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)


def get_qiniu_token():
    """
    获取七牛云token
    """
    q = Auth(access_key, secret_key)
    # 要上传的空间
    bucket_name = 'testwangyufei'
    # 生成上传 Token
    token = q.upload_token(bucket_name)
    return token


class LargeResultsSetPagination(PageNumberPagination):
    """
    自定义分页器
    """
    # 默认每页显示10条数据
    page_size = 10

    # 根据'page_size'关键字自定义每页显示多少条数据
    page_size_query_param = 'size'

    # 自定义'pag'关键字查询具体某一页的数据（默认为'page'）
    # 如：?pag=3 即查询第三页的数据
    page_query_param = 'page'

    # 每页最多显示15条数据
    # 克制 ?page_size=20,这样自定义无效，每页最多显示15条数据
    # max_page_size = 5