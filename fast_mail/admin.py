from django.contrib import admin

from fast_mail.models import Truck, Driver


# Register your models here.

@admin.register(Driver)
class DriverAdmin(admin.ModelAdmin):
    """
    司机后台管理
    """
    list_display = ('name',)  # 展示
    list_filter = ('name',)
    list_per_page = 4  # 分页
    search_fields = ('name',)


@admin.register(Truck)
class TruckAdmin(admin.ModelAdmin):
    """
    货车后台管理
    """
    list_display = ('plate_number', )  # 展示
    list_filter = ('plate_number',)
    list_per_page = 4  # 分页
    search_fields = ('plate_number',)
