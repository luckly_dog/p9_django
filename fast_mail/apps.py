from django.apps import AppConfig


class FastMailConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fast_mail'
