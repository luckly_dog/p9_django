"""
序列化器
"""

from fast_mail.models import Driver, Truck, MailRecord
from rest_framework import serializers


class DriverSerializer(serializers.ModelSerializer):
    """用户序列化器"""

    status = serializers.SerializerMethodField()

    class Meta:
        model = Driver
        fields = '__all__'

    def get_status(self, obj):
        return '空闲' if obj.status == 0 else '繁忙'


class TruckSerializer(serializers.ModelSerializer):
    """用户序列化器"""

    status = serializers.SerializerMethodField()

    class Meta:
        model = Truck
        fields = '__all__'

    def get_status(self, obj):
        return '空闲' if obj.status == 0 else '繁忙'


class MailRecordSerializer(serializers.ModelSerializer):
    """用户序列化器"""

    driver_name = serializers.SerializerMethodField()
    driver_phone = serializers.SerializerMethodField()
    truck = serializers.SerializerMethodField()

    class Meta:
        model = MailRecord
        fields = '__all__'

    def get_driver_name(self, obj):
        return obj.driver.name if obj.driver else '-'

    def get_driver_phone(self, obj):
        return obj.driver.phone if obj.driver else '-'

    def get_truck(self, obj):
        return obj.truck.plate_number if obj.truck else '-'
