
from django.urls import path

from fast_mail import views

urlpatterns = [
    path('driver_list', views.GetDriverList.as_view(), name='driver_list'),
    path('truck_list', views.GetTruckList.as_view(), name='truck_list'),
    path('mail_list', views.GetMail.as_view(), name='mail_list'),
    path('add_fast_mail', views.AddFastMail.as_view(), name='add_fast_mail'),

]
