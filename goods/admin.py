from django.contrib import admin

# Register your models here.
from goods.models import Goods, Picture


@admin.register(Goods)
class GoodsAdmin(admin.ModelAdmin):
    list_display = ('name',)  # 展示
    list_filter = ('name',)
    list_per_page = 4  # 分页
    search_fields = ('name',)


@admin.register(Picture)
class PictureAdmin(admin.ModelAdmin):
    list_display = ('goods_id', 'url_path')  # 展示
    list_filter = ('goods_id', 'url_path')
    list_per_page = 4  # 分页
    search_fields = ('goods_id', 'url_path')
