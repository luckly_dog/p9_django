from django.db import models
from django.utils import timezone

from account.models import Account
# Create your models here.


class Goods(models.Model):
    """
    商品表
    """
    name = models.CharField(max_length=30, verbose_name='商品名称')
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    count = models.IntegerField(default=0, verbose_name='库存数量')
    lock_count = models.IntegerField(default=0, verbose_name='锁定数量')
    sales = models.IntegerField(default=0, verbose_name='已经出售数量')
    view_count = models.IntegerField(default=0, verbose_name='浏览量')
    comment_count = models.IntegerField(default=0, verbose_name='评论量')
    desc = models.TextField(verbose_name='商品描述', default='')
    # main_picture = models.CharField(max_length=100, verbose_name='主图片地址')

    class Meta:
        """
        重命名
        """
        db_table = 'goods_goods'

    def get_available_count(self):
        """
        获取可用数据
        """
        return self.count - self.lock_count

    def add_lock_count(self, lock_count):
        """
        添加锁定数量
        """
        self.lock_count += lock_count
        self.save()

    def reduce_lock_count(self, lock_count):
        """
        减少锁定数量
        """
        self.lock_count -= lock_count
        self.save()


class Picture(models.Model):
    """
    图片地址
    """
    goods_id = models.IntegerField(default=0, verbose_name='关联的产品id')
    url_path = models.CharField(max_length=200, blank=True, null=True, verbose_name='图片视频地址')

    class Meta:
        """
        重命名
        """
        db_table = 'goods_picture'


class GoodsOrder(models.Model):
    """
    商品订单
    """
    status_choice = {
        0: '待支付',
        1: '支付中',
        2: '完成支付',
        3: '取消支付',
        4: '支付超时',
        9: '支付异常'
    }

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    order_id = models.CharField(max_length=20, verbose_name='订单号')
    pay_type = models.IntegerField(default=1, verbose_name='支付方式')
    total_count = models.IntegerField(default=0, verbose_name='购买商品总数量')
    total_price = models.DecimalField(verbose_name='购买商品总价格', max_digits=5, decimal_places=2)
    pay_status = models.IntegerField(default=0, verbose_name='支付状态')
    create_time = models.DateTimeField(default=timezone.now, verbose_name='订单创建时间')
    pay_time_begin = models.DateTimeField(null=True, blank=True, verbose_name='订单开始支付时间')
    pay_time_end = models.DateTimeField(null=True, blank=True, verbose_name='订单支付完成时间')
    pay_total = models.DecimalField(verbose_name='时间支付价格',  max_digits=5, decimal_places=2)
    # TODO 地址 外键

    class Meta:
        """
        重命名
        """
        db_table = 'goods_order'

    def update_pay_status(self, status):
        """
        更新订单状态
        """
        self.pay_status = status
        self.save()
        obj_list = SubGoodsOrder.objects.filter(order=self)
        obj_list.update({'pay_status': status})
        obj_list.save()

    # TODO 重新django save 方法
    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     pass


class SubGoodsOrder(models.Model):
    """
    商品子订单
    """
    order = models.ForeignKey(GoodsOrder, on_delete=models.CASCADE, verbose_name='父订单', blank=True, null=True)
    sub_order_id = models.CharField(max_length=20, verbose_name='子订单号')
    goods = models.ForeignKey(Goods, on_delete=models.CASCADE)
    count = models.IntegerField(default=1, verbose_name='商品数量')
    create_time = models.DateTimeField(default=timezone.now, verbose_name='购买日期')
    show_price = models.DecimalField(verbose_name='购买时展示的价格', max_digits=5, decimal_places=2)
    cur_total_price = models.DecimalField(verbose_name='当前子订单商品总结', max_digits=5, decimal_places=2)
    pay_price = models.DecimalField(verbose_name='真正的支付价格', max_digits=5, decimal_places=2)
    pay_status = models.IntegerField(default=0, verbose_name='支付状态')
    # TODO  快递 信息mail

    class Meta:
        """
        重命名
        """
        db_table = 'goods_sub_order'
