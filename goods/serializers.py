"""
序列化器
"""

from rest_framework import serializers
from goods.models import Goods


class GoodsSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """
    available_count = serializers.SerializerMethodField()
    id = serializers.SerializerMethodField()

    class Meta:
        """
        序列化模型
        """
        model = Goods
        fields = '__all__'

    def get_id(self, obj):
        return obj.id

    def get_available_count(self, obj):
        """
        获取可用数量
        """
        return obj.count - obj.lock_count

