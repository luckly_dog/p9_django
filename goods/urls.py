
from django.urls import path

from goods import views

urlpatterns = [
    path('add_card_goods', views.AddCartGoods.as_view(), name='add_card_goods'),
    path('update_card_goods', views.UpdateCartGoods.as_view(), name='update_card_goods'),
    path('del_card_goods', views.DeleteCartGoods.as_view(), name='del_card_goods'),
    path('get_goods_info', views.GetGoodsInfo.as_view(), name='get_goods_info'),
    path('add_goods', views.AddGoods.as_view(), name='add_goods'),
    path('search_goods', views.SearchGoods.as_view(), name='search_goods'),
    path('get_pay_url', views.GetPayURl.as_view(), name='get_pay_url'),
    path('get_pay_result', views.GetOrderResult.as_view(), name='get_pay_result'),
]
