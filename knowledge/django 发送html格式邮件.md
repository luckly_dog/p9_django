1、配置邮箱

    需要开通对应的服务，以qq邮箱为例，点击设置--》账户   中，找到对应服务

![img.png](img.png)

2、配置settings
    
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.qq.com'  # 腾讯QQ邮箱 SMTP 服务器地址
    EMAIL_PORT = 25  # SMTP服务的端口号
    EMAIL_HOST_USER = 'xxx@qq.com'  # 发送邮件的QQ邮箱
    EMAIL_HOST_PASSWORD = 'xxxx'  # 在QQ邮箱->设置->帐户->“POP3/IMAP......服务” 里得到的在第三方登录QQ邮箱授权码

3、添加html模板

    首先setting中配置html模板位置
    TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS':  [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
    ]

4、发送邮件

    subject = '测试邮件主题'
    context = {}
    from_email = 'xxx@qq.com'  # 邮件发送人
    recipient_list = ['xxx@qq.com', 'xxx@163.com']  # 邮件接收人
    
    # 这里需要提前写好html的页面
    email_template_name = 'template.html'  
    t = loader.get_template(email_template_name)  # 导入模板

    html_content = t.render(context)  # 模板参数
    msg = EmailMultiAlternatives(subject, html_content, from_email, recipient_list)
    msg.attach_alternative(html_content, "text/html")  
    msg.send()  # 发送邮件