from django.contrib import admin

# Register your models here.
from menu.models import Menu


@admin.register(Menu)
class AccountAdmin(admin.ModelAdmin):
    """
    账号后台管理
    """
    list_display = ('id', 'name', 'pid', 'status')  # 展示
    list_filter = ('name', 'pid', 'status')
    list_per_page = 20  # 分页
    search_fields = ('name', 'pid', 'status')
