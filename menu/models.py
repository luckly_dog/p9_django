from django.db import models

# Create your models here.


class Menu(models.Model):
    """
    菜单列表

    无限极模式展示
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30, verbose_name='菜单名称')
    pid = models.IntegerField(default=0, verbose_name='父id')
    status = models.IntegerField(default=1, verbose_name='1显示0不显示')

    def __str__(self):
        return self.name

    class Meta:
        """
        重命名
        """
        db_table = 'menu_menu'

