"""
序列化器
"""

from rest_framework import serializers
from menu.models import Menu


class MenuSerializer(serializers.ModelSerializer):
    """
    用户组序列化器
    """

    class Meta:
        """
        序列化模型
        """
        model = Menu
        fields = '__all__'

