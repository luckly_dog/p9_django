
from django.urls import path

from menu import views

urlpatterns = [
    path('menu_list', views.GetMenuList.as_view(), name='menu_list'),
    path('add_menu', views.AddMenu.as_view(), name='add_menu'),
    path('update', views.UpdateMenu.as_view(), name='update'),
]
