"""
通用方法
"""


def xtree(data):
    if len(data) <= 0:
        return data
    # 对数据解析重组
    tree = {}
    for i in data:
        tree[i['id']] = i

    # 初始化列表
    dlist = []

    for j in data:
        # 查看pid是否为0，为0代表第一级
        pid = j['pid']
        if pid == 0:
            dlist.append(j)
        else:
            # 判断此子类的父类下面是否已经有子类，如果没有初始化
            if not tree.get(pid, None):
                continue

            if 'children' not in tree[pid]:
                tree[pid]['children'] = []
            tree[pid]['children'].append(j)

    return dlist


def get_menu_list(data):
    """
    获取菜单列表
    """
    if len(data) <= 0:
        return data
    # 对数据解析重组
    tree = {}
    for i in data:
        tree[i['id']] = i

    # 初始化列表
    dlist = []

    for j in data:
        # 查看pid是否为0，为0代表第一级
        pid = j['pid']
        if pid == 0:
            dlist.append(j)
        else:
            # 判断此子类的父类下面是否已经有子类，如果没有初始化
            if not tree.get(pid, None):
                continue

            if 'son' not in tree[pid]:
                tree[pid]['son'] = []
            tree[pid]['son'].append(j)

    return dlist
