from django.shortcuts import render

# Create your views here.

import traceback

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render

# Create your views here.
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.decorators.cache import cache_page
from django.core.cache import cache

from menu.models import Menu
from menu.serializers import MenuSerializer
from menu.utils import xtree, get_menu_list


class GetMenuList(APIView):
    """
    获取菜单列表
    """


    def get(self, request):
        result = cache.get('menu_list', None)
        if not result:
            menu_list = Menu.objects.filter(status=1)
            menu_list = MenuSerializer(menu_list, many=True).data
            result = xtree(menu_list)
            cache.set('menu_list', result, 10)
        return Response({'code': 200, 'data': result})


class AddMenu(APIView):
    """
    添加菜单
    """
    def post(self, request):
        data = request.data
        name = data.get('name')
        pid = data.get('pid', 0)
        status = data.get('status', 1)
        try:
            if not name:
                return Response({'code': 406, 'message': 'name is null'})
            number = Menu.objects.filter(name=name).count()
            if number > 0:
                return Response({'code': 405, 'message': 'this menu name is exist'})
            if pid:
                obj = Menu.objects.filter(id=pid).first()
                if not obj:
                    return Response({'code': 407, 'message': 'pid is error'})

            menu = Menu.objects.create(name=name, pid=pid, status=status)
            menu.save()
            return Response({'code': 200, 'message': 'success'})
        except:
            error = traceback.format_exc()
            print('error', error)
            return Response({'code': 500, 'message': error})


class UpdateMenu(APIView):
    """
    修改菜单信息
    """
    def post(self, request):
        """
        1、验证修改后的名字是否存在，如果存在，返回错误信息
        2、验证id，是否存在档期数据
        """
        data = request.data
        print('2222', data)

        id = data.get('id')
        name = data.get('name')
        if not all([name, id]):
            return Response({'code': 400, 'message': 'params is error'})
        try:
            # 验证名字是否合法
            number = Menu.objects.filter(name=name).count()
            if number > 0:
                return Response({'code': 406, 'message': 'this name is exist'})
            menu = Menu.objects.filter(id=id).first()
            if not menu:
                return Response({'code': 406, 'message': 'this id is not exist'})
            menu.name = name
            menu.save()
            return Response({'code': 200, 'message': 'success'})
        except:
            error = traceback.format_exc()
            print('UpdateMenu is error:{}'.format(error))
            return Response({'code': 500, 'message': error})

