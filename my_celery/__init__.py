# 主程序

import os
from celery import Celery
from datetime import timedelta


import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()
# 创建celery实例对象
app = Celery("test")

# 通过app对象加载配置
app.config_from_object("my_celery.config")
app.autodiscover_tasks(['my_celery.schedule_tasks'])

app.conf.update(
    CELERYBEAT_SCHEDULE={
        'sum-task': {
            'task': 'my_celery.schedule_tasks.my_crontab',
            'schedule':  timedelta(seconds=5),
            'args': (5, 6)
        },

        'sum-task1': {
            'task': 'my_celery.schedule_tasks.my_print',
            'schedule':  timedelta(seconds=3),
            'args': ()
        },
    }
)


