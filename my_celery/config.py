"""
celery配置文件
"""
broker_url = 'redis://101.42.224.35:6379/15'
# 结果队列的链接地址
result_backend = 'redis://101.42.224.35:6379/14'

timezone = 'Asia/Shanghai'

# 导入指定的任务模块
imports = (
    'my_celery.tasks'
)

