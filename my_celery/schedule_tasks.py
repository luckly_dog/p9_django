"""
定时任务
"""
from django.utils import timezone

from my_celery import app
from goods.models import GoodsOrder


@app.task()
def my_crontab(x, y):
    print(x, y)
    print('x 和 y 相加 = {x+y}')
    return x + y


# 定时任务2
@app.task()
def my_print():
    print('22222222222222')


@app.task()
def check_order_pay_status():
    """
    检查订单支付状态

    1、订单超过半小时未支付，将订单状态改为支付超时
    2、用户支付成功后，回调失败或者用户异常退出，导致订单状态修改失败。
       由当前定时任务修改订单的支付状态
    """
    # TODO
    order_list = GoodsOrder.objects.filter(pay_status=1)

    for order in order_list:
        now_time = timezone.now()

        if (now_time - order.pay_time_begin).seconds > 1800:
            order.pay_status = 4
            order.save()
