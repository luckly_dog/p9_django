"""
添加任务
"""
import time

from django.core.mail import EmailMultiAlternatives

from my_celery import app


@app.task(name="send_sms")
def send_sms():
    print("发送短信!!!")


@app.task(name="send_sms2")
def send_sms2():
    print("发送短信任务2!!!")


@app.task(name="send_mail", bind=True)
def send_mail(self, subject, html_content, from_email, recipient_list):
    try:
        msg = EmailMultiAlternatives(subject, html_content, from_email, recipient_list)
        msg.attach_alternative(html_content, "text/html")
        msg.send()  # 发送邮件
    except Exception as e:
        raise self.retry(exc=e, countdown=3, max_retries=5)
