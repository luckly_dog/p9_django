from django.apps import AppConfig


class RepariConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'repair'
