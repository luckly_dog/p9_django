# Generated by Django 3.2.13 on 2022-06-01 06:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='HouseInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('build_no', models.IntegerField(default=0, verbose_name='楼号')),
                ('unit_no', models.IntegerField(default=0, verbose_name='单元号')),
                ('house_no', models.IntegerField(default=0, verbose_name='房屋号')),
                ('square_metre', models.CharField(default='', max_length=20, verbose_name='平米数')),
                ('update_time', models.DateTimeField(default=django.utils.timezone.now, verbose_name='信息更新时间')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'repair_house',
            },
        ),
    ]
