"""
物业维修相关model
"""
from django.utils import timezone
from django.db import models

from account.models import Account
# Create your models here.


class HouseInfo(models.Model):
    """
    房屋信息表
    """
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True, blank=True)
    build_no = models.IntegerField(default=0, verbose_name='楼号')
    unit_no = models.IntegerField(default=0, verbose_name='单元号')
    house_no = models.IntegerField(default=0, verbose_name='房屋号')
    square_metre = models.CharField(max_length=20, default='', verbose_name='平米数')
    update_time = models.DateTimeField(default=timezone.now, verbose_name='信息更新时间')

    def __str__(self):
        return '{}号楼{}单元{}'.format(self.build_no, self.unit_no, self.house_no)

    class Meta:
        db_table = 'repair_house'

