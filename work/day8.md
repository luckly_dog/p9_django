# 作业
    
    1、自己配置django的异步任务，并将发送邮件做成异步任务
    2、动态菜单中，将所有系统添加进去
        物流、停车场、菜单、注册登录

# 周末作业

    1、完善登录注册功能
    2、完善邮寄快递，包括对应的权限功能
    3、完善异步邮件功能
    4、邮件快递添加邮箱，发送快递后，发送邮件告知寄件人、收件人（使用异步功能）
    5、添加定时任务功能，定时对管理员发送邮件，展示当日快递的收发情况（如：收件xxx，发送xxx，
    到达：xxx）
    
    